# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://lukamravinec@bitbucket.org/lukamravinec/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/lukamravinec/stroboskop/commits/3e62ea451eac84bdb6b30b5fb554578c3e9eecae?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/lukamravinec/stroboskop/commits/bfdd14f05258b121a495b1b1ca15208598281168?at=izgled

Naloga 6.3.2:
https://bitbucket.org/lukamravinec/stroboskop/commits/ad41f26abb454ee0ab219b14104c2f0c8abbd797?at=izgled

Naloga 6.3.3:
https://bitbucket.org/lukamravinec/stroboskop/commits/2acfad2dddc671c83db8ce132305bddcf95248cf?at=izgled

Naloga 6.3.4:
https://bitbucket.org/lukamravinec/stroboskop/commits/a946f870b43137b060c0e56ac7ba78f1bb0af1a4?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/lukamravinec/stroboskop/commits/9566bcba43ce371bf767fdd637b602e770dc10e7

Naloga 6.4.2:
https://bitbucket.org/lukamravinec/stroboskop/commits/a0b49923cb6545038282183d25225f7d2972fbe2

Naloga 6.4.3:
https://bitbucket.org/lukamravinec/stroboskop/commits/718f3d07a2768feb7f6a1b9a2a11b3289911ee9b

Naloga 6.4.4:
https://bitbucket.org/lukamravinec/stroboskop/commits/7bbfc971d5df6cbf7fca8f872edd3385fa34a511